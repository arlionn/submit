> &#x1F353; 点击右上方的【编辑】按钮即可查看本文原始 Markdown 文档，以便了解格式。   
> &emsp;    
> &#x1F34F; 亦可[「点击此处」](https://gitee.com/lianxh/submit/blob/master/%2001B-%E6%8E%A8%E6%96%87%E6%A8%A1%E6%9D%BF-%E6%BA%90%E6%96%87%E6%A1%A3.txt) 直接查看本文原始文档。

&emsp;

> **作者**： xxx (xxx大学)        
> **邮箱**： <xxx@xxx>   

> **Source:** 原文作者, 年份(若有), 标题. [-Link-](链接地址)

例如：

- [Stata 中 dofile 编辑器的配置 —— 来个漂亮的编辑器](https://www.lianxh.cn/news/24649ba04f238.html)
- Michael R. M. Abrigo, Inessa Love, 2016, Estimation of Panel Vector Autoregression in Stata, Stata Journal, 16(3): 778–804. [-PDF-](https://sci-hub.ren/10.1177/1536867X1601600314)


&emsp;


---

**目录**

[TOC]

---

> **Note：** 不同的编辑器用以标记目录的符号不同，大致有如下几种：`[toc]`，`[TOC]`，`[[TOC]]`，可以酌情调整。

&emsp;   

## 1. 简介

连玉君 (2020) 提到：我用 Stata 已经 15 年了，最常用的命令是 `regress` 和 `sum`。

> ==格式说明：==
> 1. 中英文混排时，数字和英文字符两侧要保留一个空格
> 2. Stata 命令要用高亮方式显示，即 ``

数学公式如下：

$$y_{it} = x_{it}\beta + \varepsilon_{it} \quad (1)$$

其中，$y_{it}$ 为被解释变量，`\tag{#}` 用于标记公式的序号。

此模型可以用 Stata 中的 `regress` 或 `xtreg` 命令进行估计，例如：

```stata
. sysuse "auto.dta", clear
. reg price wei len 
```

其中，**price** 表示汽车价格，**wei** 是变量 **weight** 的简写。

> &emsp; **参考资料：** [Markdown-LaTeX 数学公式简介](lianxh.cn/news/845d7f5a2d977.html)；[常用公式](https://www.lianxh.cn/news/554f3e9c9f08d.html)

## 2. 二级标题
标题序号和标题文字之间保留一个空格。

### 2.1 三级标题

#### 2.1.1 四级小标题1
#### 2.1.2 四级小标题2


### 2.2 xxx

用连享会图床插入图片如下：

最简单的图片引用格式 (最常用)：

```Markdown 
![图片名称](地址)

e.g.
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会_2021Stata寒假班封面600.png)
```

包含完整信息的图片引用格式：

```Markdown 
![图片名称](地址 "说明文字")

e.g.
![连享会-2021 寒假班](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会_2021Stata寒假班封面600.png "连享会-2021 寒假班，最受欢迎的课")
```

效果如下：

![连享会-2021 寒假班](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会_2021Stata寒假班封面600.png "连享会-2021 寒假班，最受欢迎的课")



## 3. 加粗、高亮和代码块

- Stata 中的变量名用 **粗体**，写法为 `**粗体**`
- 文中的 Stata 命令用高亮显示，如 `xtreg`，写法为 \`xtreg\` 
- 多行 Stata 命令和结果用代码块

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201211225610.png)
```stata
. sysuse "auto.dta", clear
. sum price, meanonly

 Variable | Obs      Mean  Std. Dev.   Min    Max
----------+--------------------------------------
    price |  74  6165.257  2949.496   3291  15906
```

后续章节可以自行添加

## 4. 参考资料

**Note：** 
- 关键文献请通过 Google 学术 或 百度学术搜索全文，并按如下格式附上链接。
如果无法找到原文，可以采用 `[-PDF-](https://sci-hub.ren/doi)` 格式提供原文链接 (任何一篇有 DOI 的论文都可以通过 DOI 前面添加 `https://sci-hub.ren/` 网址获得其原文链接)。
- 推文中参考或借鉴的文献、博客文章甚至是网页都需要如实列示在「参考资料」小节，以示对原作者的尊重，也可以避免版权纠纷。
- 建议在此处列出连享会往期相关推文链接，可以使用 `lianxh 关键词, m` 命令快速产生对应的 Markdown 文本，贴入此处即可，参考 [lianxh 命令介绍](https://gitee.com/arlionn/lianxh)。

### 参考资料
- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [-PDF-](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)
- Hadri, K. 2000. Testing for stationarity in heterogeneous panel data. _Econometrics Journal_ 3: 148-161. [-PDF-](https://sci-hub.tw/10.1111/1368-423X.00043)
- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [-PDF-](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)

### 相关推文
> 请安装 `lianxh` 命令，安装命令为：`ssc install lianxh`    
> 产生如下推文链接对应的 Markdown 文本的 Stata 命令为：    
> &emsp; `lianxh 系数可视化 看懂 , m`
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata可视化：让他看懂我的结果！](https://www.lianxh.cn/news/01607de7be5e8.html)

