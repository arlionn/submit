
> ## 连享会 · 推文投稿主页

## 简介

[连享会](https://www.lianxh.cn) 自创立以来，已经分享了 400 多篇推文，涉及各类实证分析工具及 Stata\Python\R\Matlab 的应用。

秉承 **「知识需要分享」** 的理念，我们诚挚的邀请各位同仁一起分享实证分析中的经验和教训。
- 投稿录用推文达到三篇即可自动获取三天专题课程的助教资格，如 [Stata 暑期\寒假班](https://gitee.com/arlionn/PX)；
- 录用推文一篇即可获得一次小直播课程的免费听课资格，如 [连享会短书直播课](http://lianxh.duanshu.com)。
- 推文风格参照连享会主页 [lianxh.cn](https://www.lianxh.cn) 已发布推文。
  - 推荐使用 [Markdown](https://gitee.com/arlionn/md) 写推文，可以使用 [连享会·推文模板](https://gitee.com/lianxh/submit/blob/master/01B-推文模板.md)。[Markdown 教程](https://gitee.com/arlionn/md/wikis/Markdown%20%E7%AE%80%E4%BB%8B.md?sort_id=2012427)。 
  - Markdown 编辑器：建议采用 VScode 或 Typora，亦可在 [mdnice.com](https://www.mdnice.com/) 或 [简书](https://www.jianshu.com/) 中注册账号后使用  Markdown 编辑文档。
  - 当然，您也可以用 Word 撰写，由我们负责转换成 Markdown 文稿。 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

## 推文提交

> [连享会·推文模板指南](https://gitee.com/lianxh/submit/blob/master/01B-推文模板.md)；[推文模板2-下载](https://file.lianxh.cn/Template/01C-推文模板2-异质性分析-系数平滑可变模型.md) (右击另存)

- 方式 1：发送邮件至 <StataChina@163.com>
- 方式 2：&#x1F449; [点击提交](https://workspace.jianguoyun.com/inbox/collect/5da154fab60a4369be0592f9a49000ec/submit)，只需 1 分钟 &#x1F353;。

衷心感谢各位的支持和关注！


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

